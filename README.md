## Board Game Price Comparison ##
This app collcets data from the three bigest norwegain websites for board games and lets the user see the game title, raiting, price and if they're in stock and the statisticks connected to the game.


## Endpoints ##

/boardgames/v1/price?game=(title){&match=(full|part)
/boardgames/v1/update
/boardgames/v1/status


```/boardgames/v1/price?game=(title)
Finds the game, show the rating, prices and if in stock from the different websites if the websites has the games.	Must have a title included.
```

```/boardgames/v1/update
Collcets and updates all the data about the board games in the database. Will take forever
```

```/boardgames/v1/status
Checks the statuses of the website and database
```


## Resources ##

gamezone.no
outland.no
brettspill.no
firebase
outlock
go
goquery
hmtl




## Original plans and changes ##

The original plan:

/boardgames/v1/price?game=(title){&match=(full|part){&limit=[0-9]+}}
/boardgames/v1/dev?game=(title){&match=(full|part){&limit=[0-9]+}}
/boardgames/v1/most{?limit=[0-9]+}
/boardgames/v1/highest{?limit=[0-9]+}
/boardgames/v1/status

Price was orginally supposed to have the possibility to get all games that only matched part of the parameter, and with this limit was also supposed to be used.

Update didn't actually exist, and it was supposed to be an automatic thing, but as just implementing it took 80% of the time it was dropped.

Dev is short for development and was supposed to show price development and statistics over time for a game.

Most is short for most requested and was supposed to see which game people asked about the most, whether or not it was through price or through dev

Higest was supposed to give the user the higest rated games.



## What went well and what went wrong ##

The database is up and running, and the user can se the price diffrences between website. I was able to collect data from all three websites and combine them, and I created a system so If someone where to create statistics on prices over time they would be able to.

All other planned endpoints are not implemented, the code quality is lacking, almost no tests, not linting and lack of cloud functions.



## Hard aspects of the project ##

It was incredibly hard to get the data from the html. When I look at it now it doesn't look like it would be that hard, but the "easy" method didn't work the first few times, so I changed to a more complicated way of getting the html data, but at one point that also didn't work anymore. The small details of each website, and how they write how they do, and how I'm definetly not fluet in html. It was an accumulation of all the knowlege I lacked that made it very hard.

This project is too big for one person, and now, after doing the best I can I think it would have been too much for two people. It would have been best if we where three. Even if this was too little for three people, the we could have added something new to the project making it better.


## What our group has learned ##

As I had alot of stuff on my plate and time schedule and health, I didnt think of having any meetings and we only communicated slightly through chat. As I know that I work really well by my  selv, I underestimated how important a few proper meetings actually are. There was a clear lack of information that clearly held us back If we had tried for some meeting I would have found out that my team mate was sick much earlier and we could have solved the problem and made something that would still benefit both of us.


## Total work hours ##

Over 50 hours has been used on the project 


## Author ##

Donjetë Haziri



