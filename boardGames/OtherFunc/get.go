package OtherFunc

import(
"strconv"
"github.com/PuerkitoBio/goquery"
"boardGames/Variables"
"net/http"
)


// GetGZ gets info about boardgames from Gamezone.no
func GetGZ (w http.ResponseWriter) (data Variables.GetData){

  // For loop to go through every boardgame webpage on GameZone
  for i := 1; i <= Variables.MaxPages; i++ {

    // Declaring the url
    nr := strconv.Itoa(i)
    url := Variables.GZURL
    url += nr

    // Getting the url
    doc, err := goquery.NewDocument(url)
    if err != nil {
      w.WriteHeader(http.StatusInternalServerError)
      return
    }

    // Found is used to make sure we don't continuesly get the boardgames
    //  form the first page again and again, as any number over the
    //  actuall nr of pages redirects the user to the first page
    found := false
    // Limits the search to the listed boardgames
    doc.Find(".InfoOverlay").Each(func(indexdiv2 int, body *goquery.Selection) {
      // If true, go out of the Each function
      //  so we can return the fully gathered data
      if found {
        return
      }
      // Finds the boardgames title
      body.Find(".AddHeader1").Each(func(indexdiv2 int, divTitle *goquery.Selection) {
        title := divTitle.Text()
        // If current title matches the first title it means we have gone
        //  through all the webpages, and are now redirected to the first page.
        //  We need to return out of the Each function here, and then again
        //  return out of the Each function above to actually return the data
        if len(data.Title) > 0 && title == data.Title[0] {
          found = true
          return
        }
        // If title do not match, add title to the array of title
        data.Title = append(data.Title, title)
      })
      // To make sure we don't continue and add the other values
      if found {
        return
      }
      // Finds and adds the game price
      body.Find(".AddPriceLabel").Each(func(indexdiv2 int, divPrice *goquery.Selection) {
        price := divPrice.Text()
        data.Price = append(data.Price, price)
      })
      // Finds and adds if it is in stock or not
      body.Find(".DynamicStockTooltipContainer").Each(func(indexdiv2 int, divStock *goquery.Selection) {
        stock := divStock.Text()
        data.Stock = append(data.Stock, stock)
      })
      // Finds the rating and counts how many stars it has
      count := 0
      body.Find(".icon-star").Each(func(indexdiv2 int, div000 *goquery.Selection) {
        count++
      })
      data.Rating = append(data.Rating, count)
    })
    // If finished return Object with all the information
    if found {
      return data
    }
  }

  // Have to put return outside of an if
  return data
}




// GetOut gets info about boardgames from Outland ------------------------------
func GetOut (w http.ResponseWriter) (data Variables.GetData){

  // For loop to go through every boardgame webpage on Outland
  for i := 1; i <= Variables.MaxPages; i++ {

    // Declaring the url
    nr := strconv.Itoa(i)
    url := Variables.OutURL
    url += nr

    // Getting the url
    doc, err := goquery.NewDocument(url)
    if err != nil {
      w.WriteHeader(http.StatusInternalServerError)
      return
    }

    // Checks if page is empty
    noFile := false
    doc.Find(".message").Each(func(indexdiv2 int, isPage *goquery.Selection) {
      message := isPage.Text()
      if message == "Finner ingen produkter som passer til valgene." {
        noFile = true
      }
    })
    // Returns object if Page is empty
    if noFile {
      return data
    }


    // Limits the search to the listed boardgames
    doc.Find(".product-item-link").Each(func(indexdiv2 int, body *goquery.Selection) {
      // If true, go out of the Each function
      //  so we can return the fully gathered data
      tmpUrl, ok := body.Attr("href")
      if ok {
        innerDoc, err := goquery.NewDocument(tmpUrl)
        if err != nil {
        //  w.WriteHeader(http.StatusInternalServerError)
          return
        }

        innerDoc.Find(".product-info-main").Each(func(indexdiv2 int, innerBody *goquery.Selection) {

          // Finds the boardgames title
          innerBody.Find(".base").Each(func(indexdiv2 int, divTitle *goquery.Selection) {
            title := divTitle.Text()
            data.Title = append(data.Title, title)
          })
          // Finds and adds the game price
          innerBody.Find(".special-price").Each(func(indexdiv2 int, limitPrice *goquery.Selection) {
            //  Finds and adds the game price
            limitPrice.Find(".price").Each(func(indexdiv2 int, divPrice *goquery.Selection) {
              price := divPrice.Text()
              data.Price = append(data.Price, price)
            })
          })
          if len(data.Price) < len(data.Title) {
            innerBody.Find(".price-wrapper ").Each(func(indexdiv2 int, divPrice *goquery.Selection) {
                price := divPrice.Text()
                data.Price = append(data.Price, price)
            })
          }
          // Finds and adds if it is in stock or not
          innerBody.Find(".stock-count").Each(func(indexdiv2 int, divStock *goquery.Selection) {
            stock := divStock.Text()
            data.Stock = append(data.Stock, stock)
          })
        })
      }
    })
  }

  // Returns the object
  return data
}








// GetBSLoop gets info about boardgames from Brettspill.no ---------------------
func GetBSLoop (w http.ResponseWriter, baseUrl string) (data Variables.GetData) {

  // For loop to go through every boardgame webpage
  for i := 1; i <= Variables.MaxPages; i++ {

  //  Declaring the url
  nr := strconv.Itoa(i)
  url := baseUrl
  url += nr

  // Getting the url
  doc, err := goquery.NewDocument(url)
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    return
  }

  // Checks if page is empty
  noFile := false
  doc.Find("h1").Each(func(indexdiv2 int, isPage *goquery.Selection) {
    message := isPage.Text()
    if message == "Finner ikke filen" {
      noFile = true
    }
  })
  // Returns object if Page is empty
  if noFile {
    return data
  }

      // Limits the search to the listed boardgames
  doc.Find(".woocommerce-LoopProduct-link").Each(func(indexdiv2 int, body *goquery.Selection) {
    // If true, go out of the Each function
    //  so we can return the fully gathered data
    tmpUrl, ok := body.Attr("href")
    if ok {
      innerDoc, err := goquery.NewDocument(tmpUrl)
      if err != nil {
      //  w.WriteHeader(http.StatusInternalServerError)
        return
      }
        // Finds the boardgames title
        innerDoc.Find("h1").Each(func(indexdiv2 int, divTitle *goquery.Selection) {
          title := divTitle.Text()
          data.Title = append(data.Title, title)
        })
        // Finds and adds the game price
        innerDoc.Find(".price").Each(func(indexdiv2 int, divPrice *goquery.Selection) {
          price := divPrice.Text()
          data.Price = append(data.Price, price)
        })
        // Finds and adds if it is in stock or not
        innerDoc.Find(".stock").Each(func(indexdiv2 int, divStock *goquery.Selection) {
          stock := divStock.Text()
          data.Stock = append(data.Stock, stock)
        })
      }
    })
  }

  // Returns the object
  return data
}





// GetBS collects the info about boardgames from Brettspill.no into one --------
func GetBS (w http.ResponseWriter) (data Variables.GetData) {

 var addData Variables.GetData

addData = GetBSLoop(w, Variables.BSEntKortURL)
for i := 0; i < len(addData.Title) && i < len(addData.Price) && i < len(addData.Stock); i++ {
data.Title = append(data.Title, addData.Title[i])
data.Price = append(data.Price, addData.Price[i])
data.Stock = append(data.Stock, addData.Stock[i])
}

addData = GetBSLoop(w, Variables.BSEntStratURL)
for i := 0; i < len(addData.Title) && i < len(addData.Price) && i < len(addData.Stock); i++ {
data.Title = append(data.Title, addData.Title[i])
data.Price = append(data.Price, addData.Price[i])
data.Stock = append(data.Stock, addData.Stock[i])
}

// Grayed out so the program can run smoother with much less data

// addData = GetBSLoop(w, Variables.BSVokSelURL)
// for i := 0; i < len(addData.Title) && i < len(addData.Price) && i < len(addData.Stock); i++ {
// data.Title = append(data.Title, addData.Title[i])
// data.Price = append(data.Price, addData.Price[i])
// data.Stock = append(data.Stock, addData.Stock[i])
// }
//
// addData = GetBSLoop(w, Variables.BSVokKortURL)
// for i := 0; i < len(addData.Title) && i < len(addData.Price) && i < len(addData.Stock); i++ {
// data.Title = append(data.Title, addData.Title[i])
// data.Price = append(data.Price, addData.Price[i])
// data.Stock = append(data.Stock, addData.Stock[i])
// }
//
// addData = GetBSLoop(w, Variables.BSFamStratURL)
// for i := 0; i < len(addData.Title) && i < len(addData.Price) && i < len(addData.Stock); i++ {
// data.Title = append(data.Title, addData.Title[i])
// data.Price = append(data.Price, addData.Price[i])
// data.Stock = append(data.Stock, addData.Stock[i])
// }
//
// addData = GetBSLoop(w, Variables.BSFamQuizURL)
// for i := 0; i < len(addData.Title) && i < len(addData.Price) && i < len(addData.Stock); i++ {
// data.Title = append(data.Title, addData.Title[i])
// data.Price = append(data.Price, addData.Price[i])
// data.Stock = append(data.Stock, addData.Stock[i])
//}


// Returns the object
return data
}
