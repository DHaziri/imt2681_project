package Variables

import(
    "time"
)

type GetData struct {
  Title []string `json:"title"`
  Price []string `json:"price"`
  Stock []string `json:"in stock"`
  Rating []int `json"rating"`
}


type PriceNStock struct {
  Price string `json:"price"`
  Stock string `json:"in stock"`
}


type Game struct {
  Title string `json:"title"`
  Rating int `json"rating"`
  BrettSpillNO PriceNStock `json"brettspill.no"`
  GameZone PriceNStock `json"gamezone"`
  Outland PriceNStock `json"outland"`
}

type History struct {
  UpdateDate time.Time `json:"date updated"`
  PriceBS string `json:"price"`
  StockBS string `json:"in stock"`
  PriceGZ string `json:"price"`
  StockGZ string `json:"in stock"`
  PriceOut string `json:"price"`
  StockOut string `json:"in stock"`
}



// Status struct --------------------------------------------------------------
type Status struct {
  GameZone int `json:"gamezone"`
  BrettSpillno int `json:"brettspill.no"`
  Outland int `json:"outland"`
  Database int `json:"database"`
  UpTime int `json:"uptime"`
  Version string `json:"v1"`
}
