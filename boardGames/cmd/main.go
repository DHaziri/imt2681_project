 package main

 import(
"net/http"
   "os"
   "time"
//"fmt"
   "log"

 "boardGames/EndpointsFunc"
   "boardGames/OtherFunc"
   "boardGames/Variables"

   firebase "firebase.google.com/go" // Same as python's import dependency as alias.

	"google.golang.org/api/option"

 	"golang.org/x/net/context"

//  "cloud.google.com/go/firestore"
 )



// Main function ---------------------------------------------------------------
func main() {

  // Starts time ---------------------------------------------------------------
  Variables.StartTime = time.Now().UTC()


  // Conecting to the firebase database ----------------------------------------
  Variables.Ctx = context.Background()
  sa := option.WithCredentialsFile(Variables.FirebaseJSON)
  app, err := firebase.NewApp(Variables.Ctx, nil, sa)
  if err != nil {
    log.Print(err)
	}
	Variables.Client, err = app.Firestore(Variables.Ctx)
	if err != nil {
    log.Print(err)
	}
	//defer Variables.Client.Close()



  // Sets the localhost port to 8080 -------------------------------------------
  port := os.Getenv("PORT")
    if port == "" {
    port = "8080"
  }


  //  Functions retriving the url ----------------------------------------------
  //   and starting the functions for the endpoints
  http.HandleFunc("/", OtherFunc.BadRequest)
  http.HandleFunc("/boardgames/v1/price", EndpointsFunc.Price)
  // http.HandleFunc("/boardgames/v1/dev", dev)
  // http.HandleFunc("/boardgames/v1/most", most)
  // http.HandleFunc("/boardgames/v1/highest", EndpointsFunc.Higest)
  http.HandleFunc("/boardgames/v1/update", EndpointsFunc.Update)
  http.HandleFunc("/boardgames/v1/status", EndpointsFunc.Status)
  //  Lets the user reenter input again and again
  http.ListenAndServe(":" + port, nil)
}
