module boardGames

go 1.13

require (
	cloud.google.com/go/firestore v1.1.0
	firebase.google.com/go v3.10.0+incompatible
	github.com/PuerkitoBio/goquery v1.5.0
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859
	google.golang.org/api v0.13.0
)

// +heroku goVersion go1.13
// +heroku install ./...
