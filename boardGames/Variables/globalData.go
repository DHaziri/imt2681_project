package Variables

import(
  "time"
  "cloud.google.com/go/firestore"
	"golang.org/x/net/context"

)



// StartTime global declatation -----------------------------------------------
var StartTime time.Time



// Int constansts --------------------------------------------------------------

// Limit default
const Limit int = 5

// MaxPages is to make sure we are able to go through all existing webpages
const MaxPages int = 20 // Currently very small. Both for easy testing and that my virtualbox does not have enough RAM



// Version ---------------------------------------------------------------------
const Version string = "v1"


// URL constants ---------------------------------------------------------------

// BaseGZURL is the link to check the status
const BaseGZURL string = "https://gamezone.no/"
// GZURL is the link to gamezone boardgames
const GZURL string = "https://gamezone.no/avdelinger/brettspill-nettbutikk?pageID="

// BaseOutURL is the link to check the status
const BaseOutURL string = "https://www.outland.no/"
// OutURL is the link to outland boardgames
const OutURL string = "https://www.outland.no/spill/brettspill?p="

// BaseBSURL is the link to check the status
const BaseBSURL string = "https://www.brettspill.no/"
// BSFamQuizURL is the link to get part of Brettspill.no library
const BSFamQuizURL string = "https://www.brettspill.no/produktkategori/familier/quiz-spill/page/"
// BSFamStratURL is the link to get part of Brettspill.no library
const BSFamStratURL string = "https://www.brettspill.no/produktkategori/familier/strategi-familier/page/"
// BSVokKortURL is the link to get part of Brettspill.no library
const BSVokKortURL string = "https://www.brettspill.no/produktkategori/voksne/kortspill/page/"
// BSVokSelURL is the link to get part of Brettspill.no library
const BSVokSelURL string = "https://www.brettspill.no/produktkategori/voksne/selskapsspill/page"
// BSEntStratURL is the link to get part of Brettspill.no library
const BSEntStratURL string = "https://www.brettspill.no/produktkategori/entusiaster/strategi/page"
// BSEntKortURL is the link to get part of Brettspill.no library
const BSEntKortURL string = "https://www.brettspill.no/produktkategori/entusiaster/kortspill-entusiaster/page"

// DataBaseURL to get database url
const DataBaseURL string = "https://firebase.google.com/"


// Message constants -----------------------------------------------------------

// NotLimit to explain limit is not valid
const NotLimit string = "Limit is not valid (not an int)"

// BadRequest to tell how to use app
const BadRequest string = "Try insted:"



// FirebaseJSON database constant ----------------------------------------------
const FirebaseJSON string = "./boardgames-9fd5a-firebase-adminsdk-vndjl-075ab4fd35.json"



// Firebase global variables ---------------------------------------------------

// Ctx to connect to database
var Ctx context.Context
// Client to connect to client
var Client *firestore.Client
