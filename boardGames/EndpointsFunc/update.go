package EndpointsFunc

import(
    "fmt"
    "net/http"
    "boardGames/OtherFunc"
   "boardGames/Variables"
   "encoding/json"
// "log"
  // "cloud.google.com/go/firestore"

    "google.golang.org/api/iterator"
  //  "cloud.google.com/go/firestore"
  //  firebase "firebase.google.com/go" // Same as python's import dependency as alias.

//   "google.golang.org/api/option"

//   "golang.org/x/net/context"
)


// Price gets one game and shows the different prices between stores -----------
func Price (w http.ResponseWriter, r *http.Request) {
 var games Variables.Game

 // Gets the parameters from the path ------------------------------------------
 _, temp, err := OtherFunc.SplitsPara(w, r)
 if err != nil {
   return
  } // Adds title if recived
  title := temp
  if title == "" {
    OtherFunc.BadRequest(w, r)
    return
  }


  iter := Variables.Client.Collection("games").Where("Title", "==", title).Documents(Variables.Ctx)
  for {
    doc, err := iter.Next()
    if err == iterator.Done {
      break
    }
    if err != nil {
      w.WriteHeader(http.StatusInternalServerError)
      return
    }
    doc.DataTo(&games)
  }
  if games.Title == "" {
     fmt.Fprintln(w, "Game does not exist in database")
     return
  }
  w.Header().Set("Content-Type", "application/json")
  bytes, err := json.Marshal(games)
  if err != nil {
    w.WriteHeader(http.StatusInternalServerError)
    return
  }
  w.Write(bytes)
  w.WriteHeader(http.StatusOK)

}




func Higest (w http.ResponseWriter, r *http.Request) {

// games := Variables.Client.Doc("games")
//
// docsnap, err := games.Get(Variables.Ctx)
// if err != nil {
// 	log.Print(err)
// }
// dataMap := docsnap.Data()
//
// var nyData []Variables.Games
// if err := docsnap.DataTo(&nyData); err != nil {
// 	// TODO: Handle error.
// }

//
//   ref := games.NewRef("games")
//
// results, err := ref.OrderByValue("rating").LimitToFirst(Variables.Limit).GetOrdered(Variables.Ctx)
// if err != nil {
//         log.Fatalln("Error querying database:", err)
// }
// for _, r := range results {
//         var games Variables.Game
//         if err := r.Unmarshal(&games); err != nil {
//                 log.Fatalln("Error unmarshaling result:", err)
//         }
//         fmt.Printf("%s was %games meteres tall", r.Key(), games.Rating)
// }


}









// Update initiate the update of the database
func Update (w http.ResponseWriter, r *http.Request){
 fmt.Fprintln(w, "Buckle up! This will take about 30 minutes!")
 OtherFunc.Combine(w)
 fmt.Fprintln(w, "Finished!")
}
