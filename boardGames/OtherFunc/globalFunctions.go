package OtherFunc

import(
"net/http"
"fmt"
"strings"
"boardGames/Variables"
"strconv"
)



// BadRequest returns how the user is supposed to write a path -----------------
func BadRequest (w http.ResponseWriter, r *http.Request) {
  http.Error(w, "Bad Request", http.StatusBadRequest)
  fmt.Fprintf(w, Variables.BadRequest)
}



// SplitsPara splits and returns the different parameters ----------------------
func SplitsPara (w http.ResponseWriter, r *http.Request) (int, string, error) {


  // Declare the returns
  lim := Variables.Limit
  game := ""
  var err error

  // Splits the URL parameters
  limSplits := strings.Split(r.URL.RawQuery, "limit=")
  gameSplits := strings.Split(r.URL.RawQuery, "game=")


  // If both parameters are used -----------------------------------------------
  if len(gameSplits) == 2 && len(limSplits) == 2 {

    // Splits auth of limit, and convert limit to an int
    limGameSplits := strings.Split(gameSplits[1], "&limit=")
    // Converts limit to an int
    tempLim, tempErr := strconv.Atoi(limSplits[1])
    if tempErr != nil{
      fmt.Fprintln(w, Variables.NotLimit)
      err = tempErr
    }
    // Fills inn limit and boardgame name
    lim = tempLim
    game = limGameSplits[0]

  // If only limit is used -----------------------------------------------------
  } else if len(gameSplits) == 2 {
    // Fills boardgame name
    game = gameSplits[1]
  }


  return lim, game, err
}
