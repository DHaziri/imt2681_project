package OtherFunc

import(
"fmt"
"strings"
"time"
"net/http"
// "log"
// "strconv"
// "github.com/PuerkitoBio/goquery"
"boardGames/Variables"
"google.golang.org/api/iterator"
//"cloud.google.com/go/firestore"
)


// Combine fixes the titles, combines the games and updates the database -------
func Combine (w http.ResponseWriter) {

  // Declaring the structs to be added to the database
  var games []Variables.Game
  var history []Variables.History



  // Gets the info from Game Zone ----------------------------------------------
  dataGZ := GetGZ(w)

  // For loop to transfer each game to the collective struct
  for i := 0; i < len(dataGZ.Title); i++ {

    // Temps to add to the length of the struct array
    var temp Variables.Game
    games = append(games, temp)
    var tempHis Variables.History
    history = append(history, tempHis)

     // As each website has each own way to describe each game,
     // we're removing their descriptive addons to make sure we can
     // find the same game and combine them later
     // Here we're splitting away GameZones typical,
     // but in the way of our programs, naming convention
     brettspill := strings.Split(dataGZ.Title[i], " Brettspill")
     kortspill := strings.Split(dataGZ.Title[i], " Kortspill")
     puslespill := strings.Split(dataGZ.Title[i], " Puslespill")
     norsk := strings.Split(dataGZ.Title[i], " Norsk")
     norskStrek := strings.Split(dataGZ.Title[i], " - Norsk")


     // Checks for each scenario and uses the part
     // that does not contain the split
     if len(brettspill) == 2 {
       games[i].Title = brettspill[0]

     } else if len(kortspill) == 2 {
       games[i].Title = kortspill[0]

     } else if len(puslespill) == 2 {
       games[i].Title = puslespill[0]

     }else if len(norsk) == 2 {
       games[i].Title = norsk[0]

     } else if len(norskStrek) == 2 {
       games[i].Title = norskStrek[0]

       // And if the title didn't contain one of the alternatives,
       // use the original title
     } else {
        games[i].Title = dataGZ.Title[i]
     }

     // Transfers rest of the data
     games[i].Rating = dataGZ.Rating[i]
     // Trims whitespace before transfering data
     price := strings.TrimSpace(dataGZ.Price[i])
     games[i].GameZone.Price = price
     history[i].PriceGZ = price
     // Trims whitespace before transfering data
     stock := strings.TrimSpace(dataGZ.Stock[i])
     games[i].GameZone.Stock = stock
     history[i].StockGZ = stock
     history[i].UpdateDate = time.Now()

 //fmt.Println(games)
  }





  // Gets the info from Game Zone ----------------------------------------------
  dataOut := GetOut(w)
  lenght := len(games)

  // For loop to transfer each game to the collective struct
  for i := 0; i < len(dataOut.Title) && i < len(dataOut.Price) && i < len(dataOut.Stock); i++ {
    var temp Variables.Game

    // As each website has each own way to describe each game,
    // we're removing their descriptive addons to make sure we can
    // find the same game and combine them later
    // Here we're splitting away Outlands typical,
    // but in the way of our programs, naming convention
    utvidelse := strings.Split(dataOut.Title[i], " Utvidelse")
    extention := strings.Split(dataOut.Title[i], " Extention")
    no := strings.Split(dataOut.Title[i], " NO")

    // Checks for each scenario and uses the part
    // that does not contain the split
    if len(utvidelse) == 2 {
      utvidelse[0] += " Expansion"
      temp.Title = utvidelse[0]

    } else if len(extention) == 2 {
      extention[0] += " Expansion"
      temp.Title = extention[0]

    } else if len(no) == 2 {
      temp.Title = no[0]

      // And if the title didn't contain one of the alternatives,
      // use the original title
    } else {
       temp.Title = dataOut.Title[i]
    }

    // Check if game already existsS
    found := false
    for j := 0; j < lenght; j++{

      // If game is found, add data
      if games[j].Title == temp.Title {
        // Transfers the Outland data
        // Trims whitespace before transfering data
        price := strings.TrimSpace(dataOut.Price[i])
        games[j].Outland.Price = price
        history[j].PriceOut = price
        // Trims whitespace before transfering data
        stock := strings.TrimSpace(dataOut.Stock[i])
        games[j].Outland.Stock = stock
        history[j].StockOut = stock
        history[j].UpdateDate = time.Now()
        // Make found true
        found = true
      }
    }

    // If thoughout the loop no matching game exist
    //  add new
    if !found {
      var tempHis Variables.History
      price := strings.TrimSpace(dataOut.Price[i])
      temp.Outland.Price = price
      tempHis.PriceOut = price
      // Trims whitespace before transfering data
      stock := strings.TrimSpace(dataOut.Stock[i])
      temp.Outland.Stock = stock
      tempHis.StockOut = stock
      tempHis.UpdateDate = time.Now()

      games = append(games, temp)
      history = append(history, tempHis)
    }
  }





  // Gets the info from Brettspill.no ------------------------------------------
  dataBS := GetBS(w)
  lenght = len(games)

  // For loop to transfer each game to the collective struct
  for i := 0; i < len(dataBS.Title) && i < len(dataBS.Price) && i < len(dataBS.Stock); i++ {
    var temp Variables.Game

    // As each website has each own way to describe each game,
    // we're removing their descriptive addons to make sure we can
    // find the same game and combine them later
    // Here we're splitting away Outlands typical,
    // but in the way of our programs, naming convention
    bigBS := strings.Split(dataBS.Title[i], " Brettspill")
    smallBS := strings.Split(dataBS.Title[i], " brettspill")
    kortspill := strings.Split(dataBS.Title[i], " kortspill")
    utvidelse := strings.Split(dataBS.Title[i], " utvidelse")
    smallExp := strings.Split(dataBS.Title[i], " expansion")
    exp := strings.Split(dataBS.Title[i], " exp")
    expP := strings.Split(dataBS.Title[i], " exp.")
    engelsk := strings.Split(dataBS.Title[i], " (engelsk)")
    norsk := strings.Split(dataBS.Title[i], " norsk")


    // Checks for each scenario and uses the part
    // that does not contain the split
    if len(bigBS) == 2 {
      temp.Title = bigBS[0]

    } else if len(smallBS) == 2 {
      temp.Title = smallBS[0]

    } else if len(kortspill) == 2 {
      temp.Title = kortspill[0]

    } else if len(utvidelse) == 2 {
      utvidelse[0] += " Expansion"
      temp.Title = utvidelse[0]

    } else if len(smallExp) == 2 {
      smallExp[0] += " Expansion"
      temp.Title = smallExp[0]

    } else if len(exp) == 2 {
      exp[0] += " Expansion"
      temp.Title = exp[0]

    } else if len(expP) == 2 {
      expP[0] += " Expansion"
      temp.Title = expP[0]

    } else if len(engelsk) == 2 {
      temp.Title = engelsk[0]

    } else if len(norsk) == 2 {
      temp.Title = norsk[0]

      // And if the title didn't contain one of the alternatives,
      // use the original title
    } else {
       temp.Title = dataBS.Title[i]
    }


    // Check if game already exists
    found := false
    for j := 0; j < lenght; j++{

      // If game is found, add data
      if games[j].Title == temp.Title {
        // Transfers the Brettspill.no data
        // Trims before sale price and whitespace before transfering data
        noTrim := dataBS.Price[i]
        ifSale := strings.Split(dataBS.Price[i], ")")
        if len(ifSale) == 2 {
          noTrim = ifSale[1]
        }
        price := strings.TrimSpace(noTrim)
        games[j].BrettSpillNO.Price = price
        history[j].PriceBS = price
        // Trims whitespace before transfering data
        stock := strings.TrimSpace(dataBS.Stock[i])
        games[j].BrettSpillNO.Stock = stock
        history[j].StockBS = stock
        history[j].UpdateDate = time.Now()
        // Make found true
        found = true
      }
    }

    // If thoughout the loop no matching game exist
    //  add new
    if !found {
      var tempHis Variables.History
      // Transfers the Brettspill.no data
      // Trims before sale price and whitespace before transfering data
      noTrim := dataBS.Price[i]
      ifSale := strings.Split(dataBS.Price[i], ")")
      if len(ifSale) == 2 {
        noTrim = ifSale[1]
      }
      price := strings.TrimSpace(noTrim)
      temp.BrettSpillNO.Price = price
      tempHis.PriceBS = price
      // Trims whitespace before transfering data
      stock := strings.TrimSpace(dataBS.Stock[i])
      temp.BrettSpillNO.Stock = stock
      tempHis.StockBS = stock
      tempHis.UpdateDate = time.Now()

      games = append(games, temp)
      history = append(history, tempHis)
    }
  }




  lenght = len(games)

  for i := 0; i < lenght; i++ {

  // Makes sure theres no empty names before
  // adding the games to the database
  if games[i].Title != "" {

    // Checks if game already exists in database or not
    iter := Variables.Client.Collection("games").Where("Title", "==", games[i].Title).Documents(Variables.Ctx)
    doc, err := iter.Next()
    // If it does not, Add the new game
    if err == iterator.Done {
      _, _, err := Variables.Client.Collection("games").Add(Variables.Ctx, games[i])
      if err != nil{
        fmt.Fprintln(w, err)
        fmt.Fprintln(w, "Could not add " + games[i].Title + " to data base")
       }

       // If game alredy exist, update it
     } else {
       _, err := Variables.Client.Collection("games").Doc(doc.Ref.ID).Set(Variables.Ctx, games[i])
       if err != nil{
         fmt.Fprintln(w, err)
         fmt.Fprintln(w, "Could not update " + games[i].Title + " to data base")
       }
       _, _, err = Variables.Client.Collection("games").Doc(doc.Ref.ID).Collection("store history").Add(Variables.Ctx, history[i])
       if err != nil{
         fmt.Fprintln(w, err)
         fmt.Fprintln(w, "Could not update " + games[i].Title + " history to data base")
       }
     }
   }
 }
}
